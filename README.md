The **mielho** web site is a small, very fast and efficient, very mobile friendly application accessible at http://mielho.punkish.org/. **mielho** is working visualization of a [Perfect Health Repo Desiderata](http://punkish.org/Perfect-Health-Repo-Desiderata).

Everything else should be really fast as all the info is cached as much as possible. In fact, once a resource is fetched, any resource, there is no further network traffic as long as the user doesn't refresh the browser (using the browser back and forward buttons is OK). Of course, everything mobile friendly but is also very minimalist and elegant on the desktop. The app focuses on speed an simplicity. There is no database, just text files stored as markdown. It is mobile first, extremely fast, and cached as much as possible, so really easy on data usage.

The search box at the top of the listings page is full-text search, and when adding and removing tags (by clicking on them), the search result set at any time is preserved.

*Most importantly,* every single click, every action, produces a state change in the URL that is recorded, so every single action, even typing in the search box, is bookmark-able and shareable. This app is a *first-class web citizen.*
