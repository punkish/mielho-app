var fs = require('fs');
var path = require('path');
var wordListPath = require('word-list');

var wordArray = fs.readFileSync(wordListPath, 'utf8').split('\n');

// return a random word
var tao = function(arr) {
	return arr[Math.floor(Math.random() * arr.length)];
};

var titleCase = function(word) {
	return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();
};

var randomNumber = function(min, max) {
	return Math.floor(Math.random() * (max - min)) + min;
};

var randomWordArray = function(len, arr) {
	var word_array = [];
	for (var i=0, j=len; i<j; i++) {
        word_array.push(tao(arr));
	}
	return word_array;
};

var users = ["Benetta", "Dante", "Eloise", "Esrin", "Pontiff"];

var licenses = {
	software : [
		{
			name : "Apache License 2.0",
			url : "https://opensource.org/licenses/Apache-2.0"
		},
		{
			name : "BSD 2",
			url : "https://opensource.org/licenses/BSD-2-Clause"
		},
		{
			name : "GPL 3.0",
			url : "https://opensource.org/licenses/GPL-3.0"
		},
		{
			name : "MIT License",
			url : "https://opensource.org/licenses/MIT"
		}
	],
	literature : [
		{
			name : "CC0 Public Domain Dedication",
			url : "https://creativecommons.org/publicdomain/zero/1.0/legalcode"
		},
		{
			name : "CC BY 4.0",
			url : "https://creativecommons.org/licenses/by/4.0/legalcode"
		},
		{
			name : "CC BY-SA 4.0",
			url : "https://creativecommons.org/licenses/by-sa/4.0/legalcode"
		}
	],
	data : [
		{
			name : "CC0 Public Domain Dedication",
			url : "https://creativecommons.org/publicdomain/zero/1.0/legalcode"
		}
	]
}

var master_tags = randomWordArray(40, wordArray);

var makeLesson = function() {
    var title = randomWordArray(randomNumber(2, 4), wordArray)
		.map(function(el) {
			return titleCase(el);
		})
		.join("-");

	var tags = randomWordArray(randomNumber(2, 4), master_tags).map(function(el) {
			return "- " + el;
		}).join("\n");


    var para_length = randomNumber(3, 5);
    var para = "";
    for (var i=0, j=para_length; i<j; i++) {
        para += titleCase(tao(wordArray)) + randomWordArray(randomNumber(5, 9), wordArray).join(" ") + ". ";
    }

    var txt = "---\n";
    txt += "uri: " + "/" + title + "\n";
	txt += "researcher: " + tao(users) + "\n";
    txt += "tags:\n";
    txt += tags + "\n";
    txt += "stars: [4,4,5,3,4,5,2,4,5]\n";
    txt += "---\n";
    txt += para + "\n";

    var dir = path.join(__dirname, "..", "data/projects", title);
    var file = path.join(dir, "index.md");

    fs.mkdirSync(dir);
    fs.writeFileSync(file, txt);
};

for (var i=0, j=30; i<j; i++) {
    makeLesson();
}
