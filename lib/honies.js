/* the following are available only on the server */

var config = require('../config'),
    fs = require('fs'),
    path = require('path'),
    yaml = require('yaml-front-matter'),
    lunr = require("lunr"),
    dir_data = path.join(__dirname, "..", "..", "data", "projects");

exports.katex = require("katex");

exports.idx_honies = lunr(function() {
    this.ref("id"),
    this.field("title", { boost: 10 }),
    this.field("tags"),
    this.field("body")
});

populateHoniesSearchIndex = function(honey) {
    exports.idx_honies.add({
        "id": honey.id,
        "title": honey.title,
        "tags": honey.tags.map(function(element) {return element.tag}).join(","),
        "body": honey.body
    });
};

exports.buildHoniesCache = function(mielho) {

	var honies = [];

    fs.readdirSync(dir_data).filter(function(file, index) {

        // remove entries that start with "."
        if (file.substr(0, 1) !== ".") {

            // return only those directories that have an index.md inside
            var path_to_index = path.join(dir_data, file, "index.md");
            return fs.lstatSync(path_to_index).isFile();
        }
    }).forEach(function(file, index) {

        var honey = {
            i : index,
            id : file,
            url : config.url + "/honey?id=" + file,
            title : file.replace(/-+/g, ' ').replace(/\\+/g, '')
        };

        var tags_folder = path.join(dir_data, file, "_tags");
        var tags;

        try {
            tags = fs.readdirSync(tags_folder);
        }
        catch (error) {
            // Here you get the error when the file was not found,
            // but you also get any other error

            //console.log("got an error opening _tags for " + f + ": " + error);
        }

        var stars_folder = path.join(dir_data, file, "_stars");
        var stars_obj = {};
        var stars;

        try {
            var stars_tmp = fs.readdirSync(stars_folder);
            stars_tmp.forEach(function(file, index) {
                var star_file = path.join(stars_folder, file);
                var star_val = fs.readFileSync(star_file, "utf-8");
                stars_obj.file = star_val;
            });
            stars = Object.keys(stars_obj);
        }
        catch (error) {
            // Here you get the error when the file was not found,
            // but you also get any other error

            //console.log("got an error opening _stars for " + f + ": " + error);
        }

        var filename = path.join(dir_data, file, "index.md");

        var metadata = yaml.loadFront(filename);
        if (metadata) {

            // map user-entered date to a better one using moment's great parser
            if (metadata.date) {
                honey.date = moment(metadata.date).format();
            }

            honey.user = metadata["researcher"];
            honey.stars = metadata["stars"] || stars;
            honey.tags = metadata["tags"] || tags;
            honey.body = metadata["__content"];
        }

        populateHoniesSearchIndex(honey);
        
        honey.pot = exports.populateHoneyPot(file, mielho);
        
        honies.push(honey);
    });

	return honies;
};

exports.populateHoneyPot = function(id, mielho) {

    var pot = {
        "venom" : [],
        "pollen" : [],
        "wax" : []
    };
    
    var pot_parts = Object.keys(pot);
    for (var i=0, j=pot_parts.length; i<j; i++) {
        var part = pot_parts[i];
        var part_dir = path.join(dir_data, id, part);
        var parts = [];
        
        try {
            
            if (fs.lstatSync(part_dir).isDirectory()) {
                fs.readdirSync(part_dir).filter(function(file, index) {
                    
                    // remove entries that start with "."
                    if (file.substr(0, 1) !== ".") {
        
                        // return parts inside
                        var part_file = path.join(dir_data, id, part, file);
                        var ext = path.extname(part_file).replace(/^\./, "");
                        
                        var content;
                        if (part === "wax") {
                            if (ext === "csv") {
                                content = fs.readFileSync(part_file, "utf-8");
                            }
                            else {
                                var url = "/projects/" + id + "/" + part + "/" + file;
                                content = "<a href='" + url + "'>" + file + "</a>";
                            }
                        }
                        else if (part === "venom") {
                            content = fs.readFileSync(part_file, "utf-8");
                        }
                        else if (part === "pollen") {
                            if (ext === "md") {
                                var foo = fs.readFileSync(part_file, "utf-8");
                                content = mielho.converter.makeHtml(foo);
                            }
                            else if (ext === "tex") {
                                var foo = fs.readFileSync(part_file, "utf-8");
                                content = mielho.honies.katex.renderToString(foo);
                            }
                            else if (ext === "pdf") {
                                var url = "/projects/" + id + "/" + part + "/" + file;
                                content = '<iframe src="/ViewerJS/#../' + url + '" width="100%" height="600" allowfullscreen webkitallowfullscreen></iframe>';
                            }
                        }

                        var this_part = {
                            "file" : file,
                            "content" : content,
                            "type" : honeyPotPartType(part, ext),
                            "ext" : ext
                        };
        
                        pot[part].push(this_part);
                    }
                });
            }
        }
        catch (error) {
            // Here you get the error when the file was not found,
            // but you also get any other error
        
            //console.log("got an error opening _stars for " + f + ": " + error);
        }
    }
    
    return pot;
};

var honeyPotPartType = function(part, ext) {
    var type;
    if (part === "wax") {
        if (ext in knownWax) {
            type = knownWax[ext];
        }
        else {
            type = "unknown type"; 
        }
    }
    else if (part === "venom") {
        if (ext in knownVenom) {
            type = knownVenom[ext];
        }
        else {
            type = "unknown type"; 
        }
    }
    else if (part === "pollen") {
        if (ext in knownPollen) {
            type = knownPollen[ext];
        }
        else {
            type = "unknown type"; 
        }
    }
    
    return type;
};

var knownPollen = {
    "md" : "Markdown",
    "markdown" : "Markdown",
    "tex" : "Latex",
    "doc": "Microsoft Word",
    "docx": "Microsoft Word",
    "pdf": "Adobe Acrobat"
};

var knownWax = {
    "csv": "comma separated values",
    "xls": "Microsoft Excel",
    "xlsx": "Microsoft Excel",
    "dbf" : "dBase"
};

var knownVenom = { 
    "1c": "1C",
    "accesslog": "Access logs",
    "ada": "Ada",
    "armasm": "ARM assembler",
    "arm": "ARM assembler",
    "avrasm": "AVR assembler",
    "actionscript": "ActionScript",
    "as": "ActionScript",
    "apache": "Apache",
    "apacheconf": "Apache",
    "applescript": "AppleScript",
    "osascript": "AppleScript",
    "asciidoc": "AsciiDoc",
    "adoc": "AsciiDoc",
    "aspectj": "AspectJ",
    "autohotkey": "AutoHotkey",
    "autoit": "AutoIt",
    "axapta": "Axapta",
    "bash": "Bash",
    "sh": "Bash",
    "zsh": "Bash",
    "basic": "Basic",
    "bnf": "BNF",
    "brainfuck": "Brainfuck",
    "bf": "Brainfuck",
    "cs": "C#",
    "csharp": "C#",
    "cpp": "C++",
    "c": "C++",
    "cc": "C++",
    "h": "C++",
    "hpp": "C++",
    "cal": "C/AL",
    "cos": "Cache Object Script",
    "cls": "Cache Object Script",
    "cmake": "CMake",
    "cmake.in": "CMake",
    "csp": "CSP",
    "css": "CSS",
    "capnproto": "Cap\"n Proto",
    "capnp": "Cap\"n Proto",
    "clojure": "Clojure",
    "clj": "Clojure",
    "coffeescript": "CoffeeScript",
    "coffee": "CoffeeScript",
    "cson": "CoffeeScript",
    "iced": "CoffeeScript",
    "crmsh": "Crmsh",
    "crm": "Crmsh",
    "pcmk": "Crmsh",
    "crystal": "Crystal",
    "cr": "Crystal",
    "d": "D",
    "dns": "DNS Zone file",
    "zone": "DNS Zone file",
    "bind": "DNS Zone file",
    "dos": "DOS",
    "bat": "DOS",
    "cmd": "DOS",
    "dart": "Dart",
    "delphi": "Delphi",
    "dpr": "Delphi",
    "dfm": "Delphi",
    "pas": "Delphi",
    "pascal": "Delphi",
    "freepascal": "Delphi",
    "lazarus": "Delphi",
    "lpr": "Delphi",
    "lfm": "Delphi",
    "diff": "Diff",
    "patch": "Diff",
    "django": "Django",
    "jinja": "Django",
    "dockerfile": "Dockerfile",
    "docker": "Dockerfile",
    "dts": "DTS (Device Tree)",
    "dust": "Dust",
    "dst": "Dust",
    "elixir": "Elixir",
    "elm": "Elm",
    "erlang": "Erlang",
    "erl": "Erlang",
    "fsharp": "F#",
    "fs": "F#",
    "fix": "FIX",
    "fortran": "Fortran",
    "f90": "Fortran",
    "f95": "Fortran",
    "gcode": "G-Code",
    "nc": "G-Code",
    "gams": "Gams",
    "gms": "Gams",
    "gauss": "GAUSS",
    "gss": "GAUSS",
    "gherkin": "Gherkin",
    "go": "Go",
    "golang": "Go",
    "golo": "Golo",
    "gololang": "Golo",
    "gradle": "Gradle",
    "groovy": "Groovy",
    "xml": "HTML XML",
    "html": "HTML XML",
    "xhtml": "HTML XML",
    "rss": "HTML XML",
    "atom": "HTML XML",
    "xjb": "HTML XML",
    "xsd": "HTML XML",
    "xsl": "HTML XML",
    "plist": "HTML XML",
    "http": "HTTP",
    "https": "HTTP",
    "haml": "Haml",
    "handlebars": "Handlebars",
    "hbs": "Handlebars",
    "html.hbs": "Handlebars",
    "html.handlebars": "Handlebars",
    "haskell": "Haskell",
    "hs": "Haskell",
    "haxe": "Haxe",
    "hx": "Haxe",
    "ini": "Ini",
    "inform7": "Inform7",
    "i7": "Inform7",
    "irpf90": "IRPF90",
    "json": "JSON",
    "java": "Java",
    "jsp": "Java",
    "javascript": "JavaScript",
    "js": "JavaScript",
    "jsx": "JavaScript",
    "lasso": "Lasso",
    "ls": "LiveScript",
    "lassoscript": "Lasso",
    "less": "Less",
    "lisp": "Lisp",
    "livecodeserver": "LiveCode Server",
    "livescript": "LiveScript",
    "lua": "Lua",
    "makefile": "Makefile",
    "mk": "Makefile",
    "mak": "Makefile",
    "markdown": "Markdown",
    "md": "Markdown",
    "mkdown": "Markdown",
    "mkd": "Markdown",
    "mathematica": "Mathematica",
    "mma": "Mathematica",
    "matlab": "Matlab",
    "maxima": "Maxima",
    "mel": "Maya Embedded Language",
    "mercury": "Mercury",
    "mizar": "Mizar",
    "mojolicious": "Mojolicious",
    "monkey": "Monkey",
    "moonscript": "Moonscript",
    "moon": "Moonscript",
    "nsis": "NSIS",
    "nginx": "Nginx",
    "nginxconf": "Nginx",
    "nimrod": "Nimrod",
    "nim": "Nimrod",
    "nix": "Nix",
    "ocaml": "OCaml",
    "ml": "OCaml",
    "objectivec": "Objective C",
    "mm": "Objective C",
    "objc": "Objective C",
    "NaN": "Objective C",
    "glsl": "OpenGL Shading Language",
    "openscad": "OpenSCAD",
    "scad": "OpenSCAD",
    "ruleslanguage": "Oracle Rules Language",
    "oxygene": "Oxygene",
    "pf": "PF",
    "pf.conf": "PF",
    "php": "PHP",
    "php3": "PHP",
    "php4": "PHP",
    "php5": "PHP",
    "php6": "PHP",
    "parser3": "Parser3",
    "perl": "Perl",
    "pl": "Perl",
    "pm": "Perl",
    "powershell": "PowerShell",
    "ps": "PowerShell",
    "processing": "Processing",
    "prolog": "Prolog",
    "protobuf": "Protocol Buffers",
    "puppet": "Puppet",
    "pp": "Puppet",
    "python": "Python",
    "py": "Python",
    "gyp": "Python",
    "profile": "Python profiler results",
    "k": "Q",
    "kdb": "Q",
    "qml": "QML",
    "r": "R",
    "rib": "RenderMan RIB",
    "rsl": "RenderMan RSL",
    "graph": "Roboconf",
    "instances": "Roboconf",
    "ruby": "Ruby",
    "rb": "Ruby",
    "gemspec": "Ruby",
    "podspec": "Ruby",
    "thor": "Ruby",
    "irb": "Ruby",
    "rust": "Rust",
    "rs": "Rust",
    "scss": "SCSS",
    "sql": "SQL",
    "p21": "STEP Part 21",
    "step": "STEP Part 21",
    "stp": "STEP Part 21",
    "scala": "Scala",
    "scheme": "Scheme",
    "scilab": "Scilab",
    "sci": "Scilab",
    "smali": "Smali",
    "smalltalk": "Smalltalk",
    "st": "Smalltalk",
    "stan": "Stan",
    "stata": "Stata",
    "stylus": "Stylus",
    "styl": "Stylus",
    "swift": "Swift",
    "tcl": "Tcl",
    "tk": "Tcl",
    "tex": "TeX",
    "thrift": "Thrift",
    "tp": "TP",
    "twig": "Twig",
    "craftcms": "Twig",
    "typescript": "TypeScript",
    "ts": "TypeScript",
    "vbnet": "VB.Net",
    "vb": "VB.Net",
    "vbscript": "VBScript",
    "vbs": "VBScript",
    "vhdl": "VHDL",
    "vala": "Vala",
    "verilog": "Verilog",
    "v": "Verilog",
    "vim": "Vim Script",
    "x86asm": "x86 Assembly",
    "xl": "XL",
    "tao": "XL",
    "xpath": "XQuery",
    "xq": "XQuery",
    "zephir": "Zephir",
    "zep": "Zephir"
};

exports.makeHtml = function(cached_data, mielho) {
    var honies = JSON.parse(JSON.stringify(cached_data));

    for (var i=0, j=honies.length; i<j; i++) {

        var txt = mielho.converter.makeHtml(honies[i].body);

        if (mielho.lightweight) {
            txt = txt.replace(
                /<img src=".*?" (.*)>/g,
                '<img src="/img/2x2.png" height="20" width="100%" border="1">'
            );
        }
        honies[i].body = txt;

        var tags = honies[i].tags.map(function(el) {
            return {
                val : el.replace(/ /g, '+'),
                dsp : el,
                cls : "not_in_focus"
            }
        });
        honies[i].tags = tags;
            
        // make the accordion closed but visible
        honies[i].status = "closed";
        
        // hide the record's details
        honies[i].visibility = "off";
    }
    
    if (honies.length == 1) {
        
        // make the accordion open
        honies[0].status = "open";
        
        // show the record's details
        honies[0].visibility = "on";
    }
    
    return {"honies" : honies};
};

/**
search for bee by id is always performed against
the entire bees collection
**/
exports.getHoniesById = function(id, honies, mielho) {
    var search_result = [];
    
    for (var i=0, j=honies.length; i<j; i++) {
        if (honies[i].id === id) {
            search_result.push(honies[i]);
            break;
        }
    }

    //console.log(search_result);
    return search_result;
};

/**
search for honies by tags is performed against a provided subset of
collection. If such a set is not provided then the entire lessons
collection is used
@param {string} tags - semi-colon separated list of tags
@param {Object[]} lessons - collection of lessons or null
**/
exports.getHoniesByTags = function(tags, honies, mielho) {
    if (tags === "all") {
        return honies;
    }
    else {            
        var search_result = null;

        var _findByTags = function(tags_arr, honies_arr) {
            var tmp_honies = [];
            var t = tags_arr.shift();

            for (var i=0, j=honies_arr.length; i<j; i++) {
                for (var k=0, l=honies_arr[i].tags.length; k<l; k++) {
                    if (honies_arr[i].tags[k] === t) {
                        tmp_honies.push(honies_arr[i]);
                    }
                }
            }

            if (tags_arr.length > 0) {
                _findByTags(tags_arr, tmp_honies);
            }
            else {
                search_result = tmp_honies;
            }
        };

        var tags_arr = tags.split(";");
        _findByTags(tags_arr, honies);
        
        return search_result;
    }
};

/**
 * search for honey by q is performed against
 * a provided subset of collection. If such a set is
 * not provided then the entire honies collection is used
 */
exports.getHoniesByQ = function(q, honies, mielho) {
    var hits = mielho.honies.idx_honies.search(q);
    var search_result = [];
    var with_score = false;

    if (with_score) {

        /*
        return an array of hashes like so

        [
            {
                // record.id === ref
                record: {}
                score: 0.36945354355361815
            }
        ]
        */

        for (var i=0, j=hits.length; i<j; i++) {
            for (var k=0, l=honies.length; k<l; k++) {
                if (hits[i].ref === honies[k].id) {
                    search_result.push({
                        hit: honies[k],
                        score : hits[i].score
                    });
                    continue;
                }
            }
        }
    }
    else {

        /*
        return an array of honies like so

        [
            honey,
            honey,
            …
        ]
        */
        for (var i=0, j=hits.length; i<j; i++) {
            for (var k=0, l=honies.length; k<l; k++) {
                if (hits[i].ref === honies[k].id) {
                    search_result.push(honies[k]);
                    continue;
                }
            }
        }
    }

    return search_result;
};

/**
search for bee by user
**/
exports.getHoniesByUser = function(user, honies, mielho) {
    var search_result = [];
    
    for (var i=0, j=honies.length; i<j; i++) {
        if (honies[i].user === user) {
            search_result.push(honies[i]);
        }
    }

    return search_result;
};
