var fs = require('fs'),
    path = require('path'),
    showdown = require('showdown'),
    dir_data = path.join(__dirname, "..", "..", "data"),
	dir_public = path.join(__dirname, "..", "public");

exports.converter = new showdown.Converter({parseImgDimensions: true});

exports.buildDataCaches = function(mielho) {
	var resources = mielho["resources"];
	for (var i in resources) {
		if (resources[i].resource_type === "static" && resources[i].cache_it) {
			resources[i].cache.full = fs.readFileSync(
				path.join(dir_data, "static-pages", i + ".md"),
				"utf-8"
			);
		}
		else if (resources[i].resource_type === "dynamic" && resources[i].cache_it) {
			var func = "build" + exports.titleCase(i) + "Cache";
			resources[i].cache.full = mielho[i][func](mielho);
		}
	}
};

/** getResource(query_object)

The query object contains the resource name and, optionally, query
parameters that can be zero or more of id, tags, or q (search term).
On the server side, we start with the entire collection of the resource
we are querying, and then filter down by each query parameter applied
in a boolean AND fashion so that the result set keeps on getting smaller.
We do the same on the client side but with two major differences:

1. We start with either the entire collection or an already queried
   collection
2. The getResource() function is called via a click event
**/
exports.getResource = function(query_object, mielho) {
	var resource = query_object.resource;
	var resources = mielho.resources;

	if (resources[resource].resource_type === "static") {
		return resources[resource].cache.full;
	}
	else if (resources[resource].resource_type === "dynamic") {

		/**
		There is one more condition to check for before embarking on querying
		for the resource—if the query request is coming from the web
		application then likely the user is already looking at a subset of
		the collection and wants to search within that collection (hence,
		the "provided" collection). This would also probably take place only
		within the browser. On the other hand, if the query is coming from an
		API call then the search will start with the entire collection. So,
		we either use the provided collection or, if no collection has been
		provided to start with, we set the starting collection to be the
		entire collection. Of course, all this only makes sense in the context
		of a "dynamic" resource.
		**/
		var collection = resources[resource].cache[query_object.starting_collection];

		/**
		If there are no query parameters present, no querying is needed so
		we send back the entire (provided) collection.
		**/

		if (exports.noQueryParams(query_object)) return collection;

		/**
		If "id" exists in the query_object, only one record is to be sent back.
		This check is always performed against the *entire* collection even if
		a starting collection is provided.
		**/
		var id = query_object.query_params.id;
		if (id) {
			var func = "get" + mielho.titleCase(resource) + "ById";
			return mielho[resource][func](id, collection);
		}

		/**
		If we've reached here, we have some filtering to do. It really doesn't
		matter in which order we filter since the result set is going to
		keep on getting smaller with each successful search.
		**/
		//var search_result = collection;
		var query_params = query_object.query_params;
		for (var query_param in query_params) {

			// since we have already searched for id, we skip it
			if (query_param !== "id") {

				var query = query_params[query_param];

				/**
				Every times the search is performed the result is assigned
				to search_result recursively, and then that is used to
				perform the next search
				**/
				var func = "get" + mielho.titleCase(resource) +
							"By" + mielho.titleCase(query_param);
				collection = mielho[resource][func](query, collection, mielho);
			}
		}
		return collection;
	}
};

/* titleCase(txt) */
exports.titleCase = function(txt) {
	return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
};

/* noQueryParams(query_object)) */
exports.noQueryParams = function(query_object) {
	var query_params = query_object.query_params;
	var qry = "";
	for (var param in query_params) {
		qry += query_params[param];
	}

	/*
	since the name of this function is negative, we send a false value
	back if there is at least one valid query param provided, and true
	if there is nothing to query
	*/
	return (qry ? false : true);
};

/**
makeQueryObjectFromRequest(req, mielho)

We construct a query_object that we can pass around with everything
we might need to carry out a query. The object looks like so

query_object = {

	// name of the resource
	"resource" : resource,

	// whether the search should start at the full collection or
	// the "currently selected" set (full | curr)
	"starting_collection" : starting_collection,

	// k,v pairs of the actual query params gleaned from req.uri
	"query_params" : {}
}
**/
exports.makeQueryObjectFromRequest = function(req, mielho) {
	var resources = mielho.resources;
	var resource = req.params["resource"] || "index";
	var starting_collection = req.query.start || "full";
	var query_params = {};

	resources[resource].valid_query_params.forEach(function(element, index) {
		if (req.query[element]) {
			query_params[element] = req.query[element];
		}
	});

	return {
		"resource" : resource,
		"starting_collection" : starting_collection,
		"query_params" : query_params
	}
};

/**
{
	num : num,
	total : total,
	tags : [{val : val, dsp : dsp}],
	honies : [{
		status : status,
		id : id,
		i : index,
		title : title,
		num_of_stars : num_of_stars,
		star : star,
		tags : [{cls : cls, val : val, dsp : dsp}],
		visibility : visibility,
		user : user,
		body : body
	}]
}
**/
exports.packageDataAsHTML = function(obj) {
	var data = obj.data;
	var resource_type = obj.resource_type;
	var query_params = obj.query_params;
	var mielho = obj.mielho;

	var resource = data.resource;
	var value = data.value;

	if (resource_type === "empty") {
		return {"login" : (resource === "login" ? true : false),
			"create-account" : (resource === "create-account" ? true : false),
			"accounts-form-type" : resource
		};
	}
	else if (resource_type === "static") {
		return {"content" : mielho.converter.makeHtml(value)};
	}
	else if (resource_type === "programmatic") {
		return {"resources" : Object.keys(mielho.resources)};
	}
	else if (resource_type === "dynamic") {
		var html = mielho[resource].makeHtml(value, mielho);

		// Add count of records
		html.num = data.value.length;
		html.total = mielho.resources[resource].cache.full.length;

		if (query_params.tags) {
			html.tags = query_params.tags.split(";").map(function(element) {
				return {"val" : element.replace(/ /g, "+"), dsp : element}
			});
		}

		return html;
	}
};